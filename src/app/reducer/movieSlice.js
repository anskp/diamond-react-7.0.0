import { createSlice } from "@reduxjs/toolkit";

const initialState = [];

const movieSlice = createSlice({
    name: "movie",
    initialState,
    reducers: {
        addMovie(state, action) {
            state.push(action.payload);
        },
        filterMovie(state, action) {
            const filterMovie = action.payload.data.filter((item) => item.original_language === action.payload.language.toLowerCase());
            state.splice(0, state.length);
            filterMovie.map((item) => {
                state.push(item);
            });
        },
    },
});

export const { addMovie, filterMovie } = movieSlice.actions;

export default movieSlice.reducer;
